package com.anikailbaciodelvampiro.group.dto;

public class FiltriCustom {

	private String fieldName;
	private Object fieldValue;
	
	private String joinFieldName;
	
	
	
	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Object getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(Object fieldValue) {
		this.fieldValue = fieldValue;
	}
}
