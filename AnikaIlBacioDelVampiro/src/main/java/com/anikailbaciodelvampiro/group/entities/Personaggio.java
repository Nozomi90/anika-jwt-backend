package com.anikailbaciodelvampiro.group.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
//import javax.persistence.Version;

@Entity
@Table(name = "personaggio")
public class Personaggio implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
			@Id
			@GeneratedValue(strategy=GenerationType.AUTO)
			@Column(name="id_personaggio")
			private 	int idPersonaggio;
			
			@Column(name="ruolo")
			private	String ruolo;
			
			@Column(name="corruttibile")
			private boolean corruttibile;		
				  
		  @Column(name="valore_ruolo")
		  private int valore_ruolo;
		  
		  	  
   	  @ManyToOne
	    @JoinColumn(name="id_gara")
	    private Gara gara;
		  
//		  @Version
//		  private int version;

			public int getIdPersonaggio() {
				return idPersonaggio;
			}

			public void setIdPersonaggio(int idPersonaggio) {
				this.idPersonaggio = idPersonaggio;
			}

			public String getRuolo() {
				return ruolo;
			}

			public void setRuolo(String ruolo) {
				this.ruolo = ruolo;
			}

			public boolean isCorruttibile() {
				return corruttibile;
			}

			public void setCorruttibile(boolean corruttibile) {
				this.corruttibile = corruttibile;
			}

			public int getValore_ruolo() {
				return valore_ruolo;
			}

			public void setValore_ruolo(int valore_ruolo) {
				this.valore_ruolo = valore_ruolo;
			}

//			public int getVersion() {
//				return version;
//			}
//
//			public void setVersion(int version) {
//				this.version = version;
//			}
		  

}
