package com.anikailbaciodelvampiro.group.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;


@Entity
@Table(name = "gara")
public class Gara implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_gara")
	private int idGara;
	
	@OneToMany(mappedBy ="gara")
	List<User> users;
	
	@OneToMany(mappedBy = "gara")
	private List<Personaggio> personaggi;
	
	@Version
	private int version;
	

	public int getIdGara() {
		return idGara;
	}

	public void setIdGara(int idGara) {
		this.idGara = idGara;
	}

	public List<Personaggio> getPersonaggi() {
		return personaggi;
	}

	public void setPersonaggi(List<Personaggio> personaggi) {
		this.personaggi = personaggi;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
