package com.anikailbaciodelvampiro.group.message.response;

public class ResponseMessage {
	private int code;
	private String message;
	private Object body;

	public ResponseMessage() {}
	
	public ResponseMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public Object getBody() {
		return body;
	}

	public void setBody(Object body) {
		this.body = body;
	}
	
}
