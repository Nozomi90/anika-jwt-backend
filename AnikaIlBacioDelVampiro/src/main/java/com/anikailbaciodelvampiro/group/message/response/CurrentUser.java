package com.anikailbaciodelvampiro.group.message.response;

import java.util.Collection;

import javax.persistence.Column;

import org.springframework.security.core.GrantedAuthority;

public class CurrentUser {
	private String token;
	private String type = "Bearer";
	private String username;	
	
	private int vittorieElette;	
	private int vittorieDannate;
	private int vittorieCollezionista;
	
	private Collection<? extends GrantedAuthority> authorities;
	
	public CurrentUser() {}

	public CurrentUser(String accessToken, String username, Collection<? extends GrantedAuthority> authorities,
			int vittorieElette, int vittorieDannate, int vittorieCollezionista) {
		this.token = accessToken;
		this.username = username;
		this.vittorieElette = vittorieElette;
		this.vittorieDannate = vittorieDannate;
		this.vittorieCollezionista = vittorieCollezionista;
		this.authorities = authorities;
	}

	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
    public int getVittorieElette() {
		return vittorieElette;
	}

	public void setVittorieElette(int vittorieElette) {
		this.vittorieElette = vittorieElette;
	}

	public int getVittorieDannate() {
		return vittorieDannate;
	}

	public void setVittorieDannate(int vittorieDannate) {
		this.vittorieDannate = vittorieDannate;
	}

	public int getVittorieCollezionista() {
		return vittorieCollezionista;
	}

	public void setVittorieCollezionista(int vittorieCollezionista) {
		this.vittorieCollezionista = vittorieCollezionista;
	}

		public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

		public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
			this.authorities = authorities;
		}
    
    
}