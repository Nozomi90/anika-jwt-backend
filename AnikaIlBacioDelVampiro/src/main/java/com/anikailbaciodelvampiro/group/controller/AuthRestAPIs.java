package com.anikailbaciodelvampiro.group.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.anikailbaciodelvampiro.group.message.request.LoginForm;
import com.anikailbaciodelvampiro.group.message.request.SignUpForm;
import com.anikailbaciodelvampiro.group.message.response.CurrentUser;
import com.anikailbaciodelvampiro.group.message.response.ResponseMessage;
import com.anikailbaciodelvampiro.group.entities.Role;
import com.anikailbaciodelvampiro.group.entities.RoleName;
import com.anikailbaciodelvampiro.group.entities.User;
import com.anikailbaciodelvampiro.group.repository.RoleRepository;
import com.anikailbaciodelvampiro.group.repository.UserRepository;
import com.anikailbaciodelvampiro.group.repository.UserRepositoryMulti;
import com.anikailbaciodelvampiro.group.security.jwt.JwtProvider;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthRestAPIs {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepositoryMulti userRepositoryMulti;
	
	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtProvider jwtProvider;

	@PostMapping("/signin")
	public ResponseEntity<ResponseMessage> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {
    // Passo la richiesta login
	
		try {
			// Chiedo istanza al managerAuth
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
		
		//creo il contesto di autenticazione, passando auth come paramentro
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		//creo token di autenticazione
		String token = jwtProvider.generateJwtToken(authentication);
		
		// creo un obj con dettagli utente che effettua la login
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		User user = userRepositoryMulti.findByUsername(userDetails.getUsername());
		
		// creo un currentUser, con setting dei parametri necessari 
		
		CurrentUser currentUser = new CurrentUser();
		currentUser.setAccessToken(token);
		currentUser.setUsername(user.getUsername());
		currentUser.setVittorieElette(user.getVittorieElette());
		currentUser.setVittorieDannate(user.getVittorieDannate());
		currentUser.setVittorieCollezionista(user.getVittorieCollezionista());
		currentUser.setAuthorities(userDetails.getAuthorities());

		
		//creo un responseMessage, il cui body sarà il currentUser.
		ResponseMessage responseMessage = new ResponseMessage();
		responseMessage.setCode(200);
		System.out.println(responseMessage.getCode());
		responseMessage.setBody(currentUser); 
		
		//ritorno il responseMessage con currentUser all'interno del suo body.
		return new ResponseEntity<ResponseMessage>(responseMessage,HttpStatus.OK);
		} catch(Exception exception) {
			//in caso di eccezione, set code 401 per rilevare errore di login in F_E
			ResponseMessage responseMessage = new ResponseMessage();
			responseMessage.setCode(401);
			System.out.println(responseMessage.getCode());
			
			return new ResponseEntity<ResponseMessage>(responseMessage,HttpStatus.OK);
		}
		
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpForm signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Username is already taken!"),
					HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Email is already in use!"),
					HttpStatus.BAD_REQUEST);
		}

		// Creating user's account
		User user = new User(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
				encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		strRoles.forEach(role -> {
			switch (role) {
			case "admin":
				Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(adminRole);

				break;
			case "pm":
				Role pmRole = roleRepository.findByName(RoleName.ROLE_PM)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(pmRole);

				break;
			default:
				Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(userRole);
			}
		});

		user.setRoles(roles);
		userRepository.save(user);

		return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
	}
}