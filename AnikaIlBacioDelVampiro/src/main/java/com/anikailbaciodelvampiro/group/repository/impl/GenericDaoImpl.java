package com.anikailbaciodelvampiro.group.repository.impl;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.anikailbaciodelvampiro.group.dto.FiltriCustom;
import com.anikailbaciodelvampiro.group.repository.GenericDao;



public abstract class GenericDaoImpl<T> implements GenericDao<T> {

	/**
	 * Iniezione dell'EntityManager in modo automatico.
	 */
	@PersistenceContext
	protected EntityManager em;
	
	private Class<T> type;
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostConstruct
	public void init() {

		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class) pt.getActualTypeArguments()[0];

	}
	
	public T create(final T t) {
		this.em.persist(t);
		return t;
	}

	public void delete(final Object id) {
		this.em.remove(this.em.getReference(type, id));
	}

	public T find(final Object id) {
		return (T) this.em.find(type, id);
	}

	public T update(final T t) {
		return this.em.merge(t);
	}

	@SuppressWarnings("unchecked")
	public List<T> findAllByFilters(FiltriCustom... filters) {

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		@SuppressWarnings("rawtypes")
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(type);

		@SuppressWarnings({ "rawtypes" })
		Root root = criteriaQuery.from(type);
		if (filters != null && filters.length > 0) {

			List<Predicate> predicates = new ArrayList<Predicate>();

			// Ci sono dei filtri in WHERE
			for (FiltriCustom filter : filters) {
				
				
				predicates.add(criteriaBuilder.equal(root.get(filter.getFieldName()), filter.getFieldValue()));
			}
			criteriaQuery.where(criteriaBuilder.and(predicates.toArray(new Predicate[] {})));
		}

		criteriaQuery.select(root);

		return em.createQuery(criteriaQuery).getResultList();
	}

	
		public List<T> findAll() {

			// Step1: ottenimento del CriteriaBuilder dall'EntityManager.
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

			/*
			 * Step1: ottenimento della CriteriaQuery dal CriteriaBuilder.
			 * Rappresenta una query (select fino alla versione 2.0, insert/update
			 * dalla 2.1) JPQL. Modella ogni possibile clausola per una query JPQL.
			 * 
			 */
			
			CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(type);

			/*
			 * Step 2. Creazione del criteria root per definire la root entity a
			 * partire dalla quale ha origine la navigazione.
			 */
			
			Root root = criteriaQuery.from(type);

			criteriaQuery.select(root);

			return em.createQuery(criteriaQuery).getResultList();
		}

}
