package com.anikailbaciodelvampiro.group.repository;

import java.util.List;

import com.anikailbaciodelvampiro.group.dto.FiltriCustom;


public interface GenericDao<T> {
	
	/**
	 * Interfaccia del repository generico. 
	 * Tutte le altre altre interfacce dei repository devono 
	 * implementare questa.
	 * 
	 * @author robgion
	 *
	 * @param <T>
	 */

	    T create(T t);

	    void delete(Object id);

	    T find(Object id);

	    T update(T t);
	    
	    public List <T> findAll();

	    public List<T> findAllByFilters(FiltriCustom... filters);

	}


