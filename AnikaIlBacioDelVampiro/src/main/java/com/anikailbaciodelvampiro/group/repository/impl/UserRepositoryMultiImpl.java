package com.anikailbaciodelvampiro.group.repository.impl;

import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.anikailbaciodelvampiro.group.dto.FiltriCustom;
import com.anikailbaciodelvampiro.group.entities.User;
import com.anikailbaciodelvampiro.group.repository.UserRepositoryMulti;

@Repository(value = "utenteDao")
@Scope(value ="prototype")
public class UserRepositoryMultiImpl extends GenericDaoImpl<User> implements UserRepositoryMulti{
//Ricerca per username e password
	@Override
	public List<User> findByUsernameAndPassword(String username, String password) {
	
		FiltriCustom predicateOne = new FiltriCustom();
		predicateOne.setFieldName("username");
		predicateOne.setFieldValue(username);
		
		FiltriCustom predicateTwo = new FiltriCustom();
		predicateTwo.setFieldName("password");
		predicateTwo.setFieldValue(password);
		
		List<User> user = findAllByFilters(predicateOne,predicateTwo);
		return user;
	}
	

//Ricerca per email
	@Override
	public List<User> findByEmailAndPassword(String username, String email) {
		
		FiltriCustom predicateOne = new FiltriCustom();
		predicateOne.setFieldName("username");
		predicateOne.setFieldValue(username);
		
		FiltriCustom predicateTwo = new FiltriCustom();
		predicateTwo.setFieldName("email");
		predicateTwo.setFieldValue(email);
		
		List<User> user = findAllByFilters(predicateOne,predicateTwo);
		return user;
	}

	
	@Override
	public List<User> findByPermission(int idPermission) {
		
		FiltriCustom permissionFilter = new FiltriCustom();
		
		permissionFilter.setFieldName("idPermission");
		permissionFilter.setFieldValue(idPermission);
		
		List<User> user = findAllByFilters(permissionFilter);
		return user;
	}
	
	@Override
	public User findByUsername(String username) {
		
		FiltriCustom predicateOne = new FiltriCustom();
		
		predicateOne.setFieldName("username");
		predicateOne.setFieldValue(username);
		
		List<User> user = findAllByFilters(predicateOne);
		return user.get(0);
	}

}
