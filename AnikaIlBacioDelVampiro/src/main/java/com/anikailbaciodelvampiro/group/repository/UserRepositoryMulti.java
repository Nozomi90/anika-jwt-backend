package com.anikailbaciodelvampiro.group.repository;

import java.util.List;

import com.anikailbaciodelvampiro.group.entities.User;

public interface UserRepositoryMulti extends GenericDao<User> {
	public List<User> findByUsernameAndPassword(String username, String password);
	public List<User> findByEmailAndPassword(String username, String email);
	public List<User> findByPermission(int idPermission);
	public User findByUsername(String username);

}
