package com.anikailbaciodelvampiro.group.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.anikailbaciodelvampiro.group.entities.Role;
import com.anikailbaciodelvampiro.group.entities.RoleName;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}